#Copyright 2019 INRIA George Krait (France) 

import flint  as ft
import sympy as sp
from copy import copy, deepcopy
import sys
import time
import numpy as np





#Checking Assumptions
def checking_smoothness(P,B,jac,wth=0.1): 
    """
        Checks whether a given list of polynomials P, defines a smooth curve C_P in the list of intervals B, with jac is
             a list of lists of polynomials.
             B=[ft.arb(0,5),ft.arb(0,5),ft.arb(0,5)]
             P= [[[[1,0,0],1],[[0,0,3],-1] ],   [[[0,1,0],1],[[0,0,2],-1] ]   ] # the curve x-z^3=y-z^2 =0
             jac=[[[[[0,0,0],1]]  ,[[[0,0,0],0]] , [[[0,0,2],-3]] ],[[[[0,0,0],0]],  [[[0,0,0],1]] , [[[0,0,1],-2]] ] ]
             checking_smoothness(P,B,jac)
             1

             B=[ft.arb(0,5),ft.arb(0,5),ft.arb(0,5)]
             P= [[[[2,0,0],1],[[0,0,3],-1] ],   [[[0,1,0],1],[[0,0,2],-1] ]   ]
             jac=jacobian_of_function_list(P)
             >>> checking_smoothness(P,B,jac)
             -1
    """
    M=[coefficient_matrix_list(Pi) for Pi in P]
    list_of_boxes=[B]
    smoothness=1
    while len(list_of_boxes)!=0 and width(list_of_boxes[len(list_of_boxes)-1])> wth:
              membership=1
              eval_P=[polyvalnd(Mi,list_of_boxes[0])[0] for Mi in M]
              for eval_Pi_at_B in eval_P:
                  if 0 not in eval_Pi_at_B:
                      membership=0
                      break
              eval_jac=matrixval(jac,list_of_boxes[0])

              full_rankness= checking_full_rank(eval_jac)
              if   membership==0  or full_rankness ==1:
                  list_of_boxes.remove(list_of_boxes[0])
              else:
                 new_children=subdivide(list_of_boxes[0])
                 list_of_boxes.remove(list_of_boxes[0])
                 list_of_boxes =   list_of_boxes +new_children
    if len(list_of_boxes)!=0:
        smoothness=-1
    return smoothness

def derivative_poly_list(poly_list,i): #computes the derivative of poly_list wrt i-th variable
    derivative=[]
    for term in poly_list:
       if term[0][i-1]!=0:
            derivative_mon=copy(term[0])
            derivative_mon[i-1]=term[0][i-1]-1
            derivative_coeff=term[1]*(term[0][i-1])
            derivative_term=[derivative_mon,derivative_coeff]
            derivative.append(derivative_term)
    if len(derivative)==0:
           derivative=[[[0]*len(poly_list[0][0]),0]]
    return derivative

def jacobian_of_function_list(P):
     jacobian=[]
     n=len(P[0][0][0])
     for i in range(len(P)):
         jacobian.append([])
         for j in range(n):
              jacobian[i].insert(j,derivative_poly_list(P[i],j+1))
     return jacobian

def width(B):
    maximal=max([float(Bi.rad()) for Bi in B])
    return maximal

def cartesian_product(B1,B2):
    the_product=[]
    for i in range(len(B1)):
        for j in range(len(B2)):
            the_product.append(B1[i]+B2[j])
    return the_product

def subdivide(B): #B is a list of ft.arb
     children=[]
     if len(B)==1:
         children=[[ft.arb(B[0].mid()+(B[0].rad()/2), B[0].rad()/2)],[ft.arb(B[0].mid()-(0.5*B[0].rad()),0.5* B[0].rad())]]
     else:
         B_prime=list(B)
         B_prime.remove(B_prime[len(B_prime)-1])
         B_pri_sub= subdivide(B_prime)
         B_one_sub=subdivide([B[len(B)-1]])
         children=cartesian_product(B_pri_sub,B_one_sub)
     return children

def i_minor(jac,i): #jac is a list of lists of  list_poly. i_minor(jac,i) is the i-th minor of jac
         jac_array=copy(np.array(jac))
         minor_i_array=np.delete(jac_array, i, axis=1)
         minor_i=minor_i_array.tolist()
         return minor_i

def matrixval(jac,X): #jac as i_minor and X is a list of ft.arb
         coeffs_matrix_matrix=[copy(jaci) for jaci in jac]
         evaluation_jac_X=[copy(jaci) for jaci in jac]
         for i in range(len(jac)):
             for j in range(len(jac[0])):
                 coeffs_matrix_matrix[i][j]=coefficient_matrix_list(jac[i][j])
                 evaluation_jac_X[i][j]=polyvalnd(coeffs_matrix_matrix[i][j],X)[0]
         return evaluation_jac_X

def checking_full_rank(M): #checks whether a list of lists of arb is full rank
            minors=[i_minor(M,t) for t in range(len(M[0]))]
            full_rank=0
            invertibility_of_eval_minors=[invertibility_of_a_matrix(Mi) for Mi in minors]
            if  1 in invertibility_of_eval_minors:
                full_rank=1
            return full_rank








# The solver
def gershgorin_circles(M):   #returns Gershgorin circles of an intrival matrix M
    radius= [0]
    i=0
    j=0
    while i< M.nrows():
        j=0
        while j< M.ncols():
            if i !=j:
              radius[i]=radius[i]+ abs(M[i,j])
            j=j+1
        i=i+1
        if i != M.ncols() :
           radius.insert(i,0)                     #Now we have that radius is the list of the radiuses of  The gershgorin circles
    T=[]
    i=0
    for r in radius:
         T.insert(i,[M[i,i],r])
         i=i+1
    return T

def invertibility_of_a_matrix(M):    # M is interival matrix .. invertibility_of_a_matrix(M) returns 0 if we are sure that M is singular, 1 if we are sure that M invertable and -1 if we do not  know.. The smaller width M has the more sure we are
 Answer =3
 M_array=ft.arb_mat(M)
 L=(M_array.mid()).inv(nonstop=True)
 if str(L[0,0]) == str("nan") :
       Answer =0
 if Answer==3:
        S=L*M_array
        i=0
        Gersh=gershgorin_circles(S)
        for R in Gersh:
             if (R[0]-R[1]).lower() <= 0  and  (R[0]+R[1]).upper() >=0:
                 i=1
                 break
        if i==0:
            Answer =1
        if i==1:
            Answer=-1


 return Answer

def gauss_seidel_dim1(a,b,x):
    return x.intersection(b/a)

def gauss_seidel_dim1p(a,b,x):
    return b/a

def gauss_seidel_dimnp(A,b,x,z):
            x_prime=list(x)

            for i in range(len(x)):
                x_i_prime=gauss_seidel_dim1p(A[i][i],b[i]-np.dot(A[i],x_prime)+A[i][i]*x_prime[i],z[i])
                x_prime[i]=x_i_prime
                #print(x_prime)
            return(x_prime)

def gauss_seidel_dimn(A,b,x,z): #A,b,x,z=lists (matrix) of ft.arb
    x_prime=list(x)
    A_arb_mat=ft.arb_mat(A)
    inv_m_A=(A_arb_mat.mid()).inv()
    A_precon=inv_m_A*ft.arb_mat(A)
    b_arb_mat=ft.arb_mat([[bi] for bi in b])
    b_precon=inv_m_A*b_arb_mat

    for i in range(len(x)):
       sum=0
       for j in range(len(x)):
           if i !=j:
               sum+=A_precon[i,j]*x_prime[j]
       x_prime[i]=gauss_seidel_dim1(A_precon[i,i],b_precon[i,0]-sum,z[i])

    return x_prime

def hansen_hengupta(x_teld,A,b,x,z):
    """
         It return the Hansen-Hengupta output operator
         x_teld is a list of float (prefered the center of x)
         x,z are lists of arb
         A is a list of lists of arb

    """
    Gamma=gauss_seidel_dimn(A,[-bi for bi in b],[xi-x_teldi for xi, x_teldi in zip(x,x_teld)],[zi-x_teldi for zi,x_teldi in zip(z,x_teld)])
    return [x + y for x, y in zip(Gamma, x_teld)] #x_teld is a list of float A is a list of list of intervals, b,x,z list of interval



#evaluation of a interval objects
def eliminate_the_last_variable(M):
    N=M
    if type(M[0])!= list:
        N.remove(M[len(N)-1])
    else:
        N=[eliminate_the_last_variable(Mi) for Mi in M]
    return N # No use so far

    #Evaluating polynomials \not used yet

def polyvalnd(M,X):
    """
         It computes the the evaluation of
         a function whose M as its coefficients matrix at B
         X=[ft.arb(0,2),ft.arb(0,2),ft.arb(0,2)]
         L=[[[[1,0,0],1],[[0,0,3],-1]],[[[0,1,0],1],[[0,0,2],-1]]]
         M=coefficient_matrix_list(L)
         >>> polyvalnd(M,X)
         [+/- 92.1]
    """
    evaluation=0

    if len(X)==1:
        evaluation= np.polynomial.polynomial.polyval(X,M)

    else:
        Y=X[0:len(X)-1]
        N=[polyvalnd(Mi,Y) for Mi in M]
        evaluation=np.polynomial.polynomial.polyval(X[len(X)-1],N)
    return evaluation





def empty_list_generator(m,n):
    """
        it  generates a n-dim list of m zeros
        >>> empty_list_generator(2,3)
        [[[0, 0, 0], [0, 0, 0], [0, 0, 0]], [[0, 0, 0], [0, 0, 0], [0, 0, 0]], [[0, 0, 0], [0, 0, 0], [0, 0, 0]]]
    """
    M=[0]*(m+1)
    N=M
    if n>1:
      for j in range(len(M)):
               N[j]=empty_list_generator(m,n-1)

      M=N

    return M

def replacing_an_elements_of_list(the_index,the_list,x):
     if len(the_index)==1:
         the_list[the_index[0]]=x

     else:
          the_new_index=the_index[1:len(the_index)]
          the_new_list=the_list[the_index[0]]
          replacing_an_elements_of_list(the_new_index,the_new_list,x)

def reversing_list(M): # not used yet
    N=M
    if type(N[0])==type([]):
        for T in N:
            T=(reversing_list(T))


    return(N)

def coefficient_matrix(f):
       non_zero_terms= f.terms()
       max_degree=0
       for T in non_zero_terms:
           if max_degree < max(list(T[0])):
               max_degree=max(list(T[0]))
       M=empty_list_generator(max_degree, len(list(T[0])))
       for T in non_zero_terms:
           N=list(T[0])
           N.reverse()
           replacing_an_elements_of_list(N,M,T[1])
           #L=reversing_list(M)
       return M

def coefficient_matrix_list(list_poly):
       """
             It returns the coefficients matrix of a list_poly
             f=[[[0,1,0],2],[[0,0,2],-1],[[0,1,2],-7],[[1,2,2],-1] ]
             >>> coefficient_matrix_list(f)
             [[[0, 0, 0], [2, 0, 0], [0, 0, 0]], [[0, 0, 0], [0, 0, 0], [0, 0, 0]], [[-1, 0, 0], [-7, 0, 0], [0, -1, 0]]]
       """


       max_degree=0
       for T in list_poly:
           if max_degree < max(list(T[0])):
               max_degree=max(list(T[0]))
       M=empty_list_generator(max_degree, len(list(T[0])))
       for T in list_poly:
           N=list(T[0])
           N.reverse()
           replacing_an_elements_of_list(N,M,T[1])
       return M

       #Ball Sysyem





#Ball system
def Ball_interval(list_polys):
    """
        Computes the Ball system as a list of terms
        >>> Ball_interval([[[[1,0,0],1],[[0,0,3],-1] ],   [[[0,1,0],1],[[0,0,2],-1] ]  ])
        [[[[1, 0, 0, 0, 0], 1.00000000000000], [[0, 0, 3, 0, 0], -1.00000000000000], [[0, 0, 1, 2, 1], -3.00000000000000]], [[[0, 1, 0, 0, 0], 1.00000000000000], [[0, 0, 2, 0, 0], -1.00000000000000], [[0, 0, 0, 2, 1], -1.00000000000000]], [[[0, 0, 0, 0, 0], 0], [[0, 0, 2, 1, 0], -3.00000000000000], [[0, 0, 0, 3, 1], -1.00000000000000]], [[[0, 0, 0, 0, 0], 0], [[0, 0, 1, 1, 0], -2.00000000000000]], [[[0, 0, 0, 2, 0], 1], [[0, 0, 0, 0, 0], -1]]]
    """
    X=genvars(len(list_polys[0][0][0]))
    Ball_system1=[]
    Ball_system2=[]
    for list_poly in list_polys:
        SD_list_poly=Ball_for_interval_poly(list_poly,X)
        Ball_system1.append(SD_list_poly[0])  #SP's
        Ball_system2.append(SD_list_poly[1])   #DP's
    last_eq=[]
    for r in X[1]:
          Ter=sp.poly(r**2,*X[0],*X[1],X[2]).terms()
          Ter[0]=list(Ter[0])
          Ter[0][0]=list(Ter[0][0])
          last_eq.append(Ter[0])
    last_eq.append([[0]*(2*len(X[0])-1),-1])
    return [*Ball_system1,*Ball_system2,last_eq]

def genvars(n):     #already exists in Ball lib
  x=[]
  r=[]
  i = 1
  while i < int(n)+1:
    x.insert(i, sp.Symbol('x'+str(i)) )
    if i>2 :
        r.insert(i, sp.Symbol('r'+str(i)) )
    i= i+1
  t=sp.Symbol('t')
  return [x,r,t]

def inBall(P,X):     #already exists in Ball lib
 i=2
 x=X[0]
 r=X[1]
 t=X[2]
 SPp=P
 DPm=P
 while i< len(x):
    SPp=SPp.subs(x[i],x[i]+r[i-2]*sp.sqrt(t))
    DPm=DPm.subs(x[i],x[i]-r[i-2]*sp.sqrt(t))
    i=i+1

 SPp=sp.expand(SPp)
 DPm=sp.expand(DPm)
 SP=sp.expand(0.5*(SPp+DPm))
 DP=sp.expand(0.5*(SPp-DPm)/sp.sqrt(t))
 return [SP,DP]

def removing_duplicated_terms(list_poly):
    list_poly_reduced=[]
    duplicated_index=[]
    k1=0
    k2=0
    k3=0
    while k1< len(list_poly):   #searching for equal monomials in  poly_SP and poly_DP
             if k1 not in duplicated_index:
                  list_poly_reduced.append(list_poly[k1])
                  k3+=1
                  k2=k1+1
                  while k2< len(list_poly) :
                     if list_poly[k3-1][0]==list_poly[k2][0]:
                       duplicated_index.append(k2)
                       list_poly_reduced[k3-1][1]+=list_poly[k2][1]
                       k2 =k2+1
                     else:
                        k2=k2+1

                  k1=k1+1

             else :
                k1+=1
    return(list_poly_reduced)

def Ball_for_terms(L,X):  #L=[list of integers,a coefficient] X is list of variables
    P=1
    for i in range(len(L[0])) :
        P=P*X[0][i]**(L[0][i])
    M= inBall(P,X)
    S=[sp.poly(T,*X[0],*X[1],X[2]).terms() for T in  M]
    S[0][0]=list(S[0][0])
    for i in  range(len(S)):
        for j in range(len(S[i])):
              S[i][j]=list(S[i][j])
              S[i][j][0]=list(S[i][j][0])
              S[i][j][1]=L[1]*S[i][j][1]
    return S

def Ball_for_interval_poly(list_of_terms,X):
    poly_SP=[]
    poly_DP=[]
    for T in list_of_terms:
        SDP=Ball_for_terms(T,X)
        poly_SP=poly_SP+SDP[0]
        poly_DP=poly_DP+SDP[1]

    return [removing_duplicated_terms(poly_SP),removing_duplicated_terms(poly_DP)]

